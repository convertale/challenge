package tr.com.iletken.fetch;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Fetch {
    private final HttpClient client;

    public Fetch() {
        ClientConnectionManager connManager = new PoolingClientConnectionManager();
        this.client = new DefaultHttpClient(connManager);
    }

    public String getHtml(String url) throws IOException {
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuilder html = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            html.append(line);
        }

        return html.toString();
    }

}
