package tr.com.iletken.job;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import tr.com.iletken.job.functions.*;

public class ChallengeJob {
    private static final String INPUT_FILE = "weather.csv";
    private static final String OUTPUT_DIRECTORY = "train_results";

    private JavaSparkContext javaSparkContext;

    public ChallengeJob() {
        SparkConf sparkConf = new SparkConf().setAppName("SparkApp");
        javaSparkContext = new JavaSparkContext(sparkConf);
    }

    public void train() {
        javaSparkContext.textFile(INPUT_FILE)
                .filter(new DateFilterFunction())
                .map(new ParserFunction())
                .mapToPair(new WeatherPairFunction())
                .reduceByKey(new WeatherReducerFunction())
                .mapToPair(new WeatherSummarizerFunction())
                .saveAsTextFile(OUTPUT_DIRECTORY);
    }

}
