package tr.com.iletken;

import tr.com.iletken.job.ChallengeJob;

public class SparkApp {

    public static void main(String[] args) {
        ChallengeJob job = new ChallengeJob();
        job.train();
    }

}