package tr.com.iletken;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import tr.com.iletken.fetch.Fetch;
import tr.com.iletken.models.Weather;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class FetchApp {

    private static final String URL = "https://www.convertale.com/challenge/intern/weather/all.html";

    private static Weather createWeather(Document doc) throws ParseException {
        Long date = null;
        Double min = null;
        Double max = null;
        Double mean = null;

        // document objesi kullanilarak sayfadaki degerlerin ilgili degiskenlere atanmasi yapilacak

        Weather weather = new Weather();

        weather.setDate(date);
        weather.setMin(min);
        weather.setMax(max);
        weather.setMean(mean);

        return weather;
    }

    public static void main(String[] args) throws IOException {
        Fetch fetch = new Fetch();
        Document doc = Jsoup.parse(fetch.getHtml(URL));
        Elements links = doc.select("a");
        List<Weather> weathers = new ArrayList<>(links.size());

        try (FileWriter fw = new FileWriter("weather.csv", true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {

            for (Element link : links) {
                doc = Jsoup.parse(fetch.getHtml(link.attr("href")));

                Weather weather = createWeather(doc);

                weathers.add(weather);

                // weather objesindeki degerler weater.csv dosyasina yazilacak
                // date,min,max,mean seklinde sirali olacak olacak.

                String line = null;

                out.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
